package pointOfSales.controller;

import java.util.List;
import java.util.Random;
import java.nio.charset.Charset;

import pointOfSales.dao.BarangDAO;
import pointOfSales.model.Barang;

public class SupervisorController {

	
	private static SupervisorController supervisorController;
	private BarangDAO barangDAO;
	
	private SupervisorController() {
		barangDAO = BarangDAO.getInstance();
	}
	
	public static synchronized SupervisorController getInstance() {
		return supervisorController = (supervisorController == null) ? new SupervisorController() : supervisorController;
	}
	
	public void addBarang(String nama, String jumlah, String harga, String pathGambar) {
		
		barangDAO.addBarang(nama, jumlah,harga, pathGambar);
	}
	
//	public String randString() {
//	    byte[] array = new byte[7]; // length is bounded by 7
//	    new Random().nextBytes(array);
//	    String generatedString = new String(array, Charset.forName("UTF-8"));
//
//	    return generatedString;
//	}
//	
	public void updateBarang(String id, String nama, String jumlah, String harga, String pathGambar) {
		barangDAO.updateBarang(id, nama, jumlah, harga, pathGambar);
	}
	
	public List<Barang> getAllBarang(){
		return barangDAO.getAll();
	}
}
