package pointOfSales.controller;

import pointOfSales.dao.*;
import pointOfSales.model.*;

import java.util.List;
import java.util.ArrayList;

public class KasirController {
	
	private static KasirController salesEntryController;
	private MemberDAO memberDAO;
	private TransaksiDAO transaksiDAO;
	private BarangDAO barangDAO;
	
	private KasirController() {
		memberDAO = MemberDAO.getInstance();
		transaksiDAO = TransaksiDAO.getInstance();
		barangDAO = BarangDAO.getInstance();
	}
	
	public static synchronized KasirController getInstance() {
		return salesEntryController = (salesEntryController == null) ? new KasirController() : salesEntryController;
	}
	
	public Boolean memberIdIsValid(String id) {
		return memberDAO.validateMember(id);
	}
	
	public List<Barang> getAllBarang(){
		return barangDAO.getAll();
	}
	
	public void approveTransaksi() {
		
	}
	
	public void saveDetailTransaksiToList(DetailTransaksi dt) {
		transaksiDAO.saveTempDetailTransaksi(dt);
	}
	
	public List<DetailTransaksi> getAllDetailTransaksi() {
		return transaksiDAO.getAllDetailTransaksi();
	}
	
	public Transaksi createTransaksi() {
		String idTransaksi = getAlphaNumericString(7);
		return new Transaksi(idTransaksi,0,0,0,0);
	}
	
	
	public String getAlphaNumericString(int n)
	    {
	  
	        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	                                    + "0123456789"
	                                    + "abcdefghijklmnopqrstuvxyz";
	        StringBuilder sb = new StringBuilder(n);
	  
	        for (int i = 0; i < n; i++) {
	            int index
	                = (int)(AlphaNumericString.length()
	                        * Math.random());
	  
	            sb.append(AlphaNumericString
	                          .charAt(index));
	        }
	  
	        return sb.toString();
	    }
}
