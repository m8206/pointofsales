package pointOfSales.utill;

import java.sql.*;

public class ConnectTest {
	
	static String DB = "Database1.accdb"; 
	static String PATHFILE = "//ms-database/";
	static String URLCON = "jdbc:ucanaccess:" + PATHFILE +DB ;
	
	public static void main(String[] args) {
		
		try {
			
			Connection connection = DriverManager.getConnection(URLCON);
			
			Statement statement = connection.createStatement();
			
			ResultSet resultSet = statement.executeQuery("select * from Barang");
			
			//result testing
			while(resultSet.next()) {
				System.out.println("\n"+resultSet.getString(1)+"\t"+resultSet.getString(2)+"\t"+resultSet.getString(3));

			}

		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
