package pointOfSales.views;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import pointOfSales.views.*;


public class Login {

	private JFrame frmIsysSimple;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	
	public void setVisiblilogin(Login windows) {
		windows = new Login();
		windows.frmIsysSimple.setVisible(true);
	}
	
	private static boolean isPasswordCorrect(char[] input, String username) {
	    boolean isCorrect = true;
	    char[] correctPasswordKasir = { 'k', 'a', 's', 'i', 'r'};
	    char[] correctPasswordSuper = { 's', 'u', 'p', 'e', 'r','v','i','s','o','r'};
	    
	    if (input.length != correctPasswordKasir.length && input.length != correctPasswordSuper.length) {
	        isCorrect = false;
	    } else if(username.equals("kasir")) {
	        isCorrect = Arrays.equals (input, correctPasswordKasir);
//	        Arrays.fill(correctPasswordKasir,'0');
	    }else if(username.equals("supervisor")) {
	    	isCorrect = Arrays.equals (input, correctPasswordSuper);
//	        Arrays.fill(correctPasswordSuper,'0');
	    }

	    //Zero out the password.
	    
	    

	    return isCorrect;
	}
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmIsysSimple.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmIsysSimple = new JFrame();
		frmIsysSimple.setTitle("Login Point of Sales");
		frmIsysSimple.setBounds(100, 100, 450, 300);
		frmIsysSimple.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frmIsysSimple.getContentPane().setLayout(gridBagLayout);
		
		JLabel lblNewLabel_1 = new JLabel("User Name  :");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 2;
		frmIsysSimple.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 3;
		gbc_textField.gridy = 2;
		frmIsysSimple.getContentPane().add(textField, gbc_textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Password   :");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 1;
		gbc_lblNewLabel_2.gridy = 4;
		frmIsysSimple.getContentPane().add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		passwordField = new JPasswordField();
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.insets = new Insets(0, 0, 5, 5);
		gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField.gridx = 3;
		gbc_passwordField.gridy = 4;
		frmIsysSimple.getContentPane().add(passwordField, gbc_passwordField);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Show Password");
		chckbxNewCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(chckbxNewCheckBox.isSelected()) {
					passwordField.setEchoChar((char)0);
				}else {
					passwordField.setEchoChar('�');
				}
			}
		});
		chckbxNewCheckBox.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.anchor = GridBagConstraints.WEST;
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNewCheckBox.gridx = 3;
		gbc_chckbxNewCheckBox.gridy = 6;
		frmIsysSimple.getContentPane().add(chckbxNewCheckBox, gbc_chckbxNewCheckBox);
		
		
		
		
		JButton btnNewButton = new JButton("LOGIN");
		btnNewButton.addActionListener(new ActionListener() {
//			String pass = new String(String.valueOf(passwordField.getPassword()));
			
			
					
			public void actionPerformed(ActionEvent e) {
				
				if(textField.getText().length() < 1 ||textField.getText().length() > 20) {
					JOptionPane.showMessageDialog(null, "Username must be between 1 - 20 characters!");
				}else if(passwordField.getPassword().length < 1 ||passwordField.getPassword().length > 20) {
					JOptionPane.showMessageDialog(null, "Password must be between 1 - 20 characters!");
				}else if(!isPasswordCorrect(passwordField.getPassword(), textField.getText())) {
					JOptionPane.showMessageDialog(null, "Wrong Password!");
				}else {
//					JOptionPane.showMessageDialog(null, "You're Log In!" + textField.getText());
					if(textField.getText().equals("kasir")) {
						Main main = new Main();
						main.setVisiblimain(main);
						frmIsysSimple.dispose();
						
					}else if(textField.getText().equals("supervisor")) {
						SupervisorPage superVisor = new SupervisorPage();
						superVisor.setVisibliSuper(superVisor);
						frmIsysSimple.dispose();
					}
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.gridwidth = 2;
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 2;
		gbc_btnNewButton.gridy = 8;
		frmIsysSimple.getContentPane().add(btnNewButton, gbc_btnNewButton);
	}

}
