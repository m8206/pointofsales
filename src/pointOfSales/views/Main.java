package pointOfSales.views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Main {

	private JFrame frmMainForm;
	private boolean approval = false;
	/**
	 * Launch the application.
	 */
	
	
	
	
	public void setVisiblimain(Main windows) {
		windows = new Main();
		windows.frmMainForm.setVisible(true);
	}
	
	
	private void btnInputDialogActionPerformed(java.awt.event.ActionEvent evt) {                                               

        
		JPanel panel = new JPanel();
		JLabel label = new JLabel("Enter a password:");
		JPasswordField pass = new JPasswordField(10);
		panel.add(label);
		panel.add(pass);
		String[] options = new String[]{"OK", "Cancel"};
		int option = JOptionPane.showOptionDialog(null, panel, "Supervisor Password",
		                         JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
		                         null, options, options[1]);
		if(option == 0) // pressing OK button
		{
		    char[] password = pass.getPassword();
		    String correctPasswordSuper = "supervisor";
		    if(correctPasswordSuper.equals(new String(password))) {
		    	btnInputDialogActionPerformed2(null);
		    }else {
		    	JOptionPane.showMessageDialog(null, "Wrong Password!");
		    }
		}
	} 
	
	
	
	private void btnInputDialogActionPerformed2(java.awt.event.ActionEvent evt) {  
		JPanel panel = new JPanel();
		JCheckBox chckbxNewCheckBox1 = new JCheckBox("Approval");
		panel.add(chckbxNewCheckBox1);
		
		String[] options = new String[]{"OK", "Cancel"};
		int option = JOptionPane.showOptionDialog(null, panel, "Supervisor Password",
		                         JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
		                         null, options, options[1]);
		
		if(option == 0) {    // pressing OK button
			if(chckbxNewCheckBox1.isSelected()) {
				approval = true;
			}else {
				approval = false;
			}
		}
		
		
	}
	
	
	private void btnInputDialogActionPerformed3(java.awt.event.ActionEvent evt) { 
		JPanel panel = new JPanel();
		JLabel label = new JLabel("Enter a password:");
		JPasswordField pass = new JPasswordField(10);
		panel.add(label);
		panel.add(pass);
		String[] options = new String[]{"OK", "Cancel"};
		int option = JOptionPane.showOptionDialog(null, panel, "Supervisor Password",
		                         JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
		                         null, options, options[1]);
		if(option == 0) // pressing OK button
		{
		    char[] password = pass.getPassword();
		    String correctPasswordSuper = "supervisor";
		    if(correctPasswordSuper.equals(new String(password))) {
		    	SupervisorPage superVisor = new SupervisorPage();
				superVisor.setVisibliSuper(superVisor);
				frmMainForm.dispose();
		    }else {
		    	JOptionPane.showMessageDialog(null, "Wrong Password!");
		    }
		}
	}
	
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmMainForm.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMainForm = new JFrame();
		frmMainForm.setTitle("Main Form");
		frmMainForm.setBounds(100, 100, 450, 300);
		frmMainForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMainForm.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Add Transaction");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Kasir kasir = new Kasir();
				kasir.setVisibliKasir(kasir);
				frmMainForm.dispose();
			}
		});
		btnNewButton.setBounds(125, 120, 185, 23);
		frmMainForm.getContentPane().add(btnNewButton);
		
		JMenuBar menuBar = new JMenuBar();
		frmMainForm.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Setting");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Help");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int jawab = JOptionPane.showOptionDialog(null,
			               "You sure need help?",
			               "Warning",
			               JOptionPane.YES_NO_OPTION,
			               JOptionPane.QUESTION_MESSAGE, null, null, null);
					
					
					 if (jawab == JOptionPane.YES_OPTION) {
						 btnInputDialogActionPerformed3(null);
				        }
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Log Out");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(approval == true) {
					JOptionPane.showMessageDialog(null, "You'll Log Off");
					Login login = new Login();
					login.setVisiblilogin(login);
					frmMainForm.dispose();
					
				}else {
					
					int jawab = JOptionPane.showOptionDialog(null,
			                "You have not been approved by your Supervisor. \nAsk approval from Supervisor?",
			                "Warning",
			                JOptionPane.YES_NO_OPTION,
			                JOptionPane.QUESTION_MESSAGE, null, null, null);
					
					
					 if (jawab == JOptionPane.YES_OPTION) {
						 btnInputDialogActionPerformed(null);
				            
				        }
				}
				
			}
		});
		mnNewMenu.add(mntmNewMenuItem_1);
	}
}
