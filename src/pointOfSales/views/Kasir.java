package pointOfSales.views;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.table.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.table.*;

import pointOfSales.controller.*;
import pointOfSales.model.Barang;
import pointOfSales.model.Transaksi;

import pointOfSales.model.DetailTransaksi;


public class Kasir {

	private JFrame frmKasir;
	private JTable tableBarang;
	private JTable tableBarangDibeli;
	private JTextField totalItemsField;
	private JTextField totalPriceField;
	private JTextField paidField;
	private JTextField changeField;
	private JTextField textId;
	JCheckBox chckbxNewCheckBox;
	private JTextField diskonField;
	private KasirController kasirController;
	private Transaksi transaksi;
//	public boolean approvalSuper;

	/**
	 * Launch the application.
	 */

	public void setVisibliKasir(Kasir windows) {
		windows = new Kasir();
		windows.frmKasir.setVisible(true);
	}

//	public void setVisible() {
//		frmKasir.setVisible(true);
//	}
//	

	private void btnInputDialogActionPerformed(java.awt.event.ActionEvent evt) {

		JPanel panel = new JPanel();
		JLabel label = new JLabel("Enter a password:");
		JPasswordField pass = new JPasswordField(10);
		panel.add(label);
		panel.add(pass);
		String[] options = new String[] { "OK", "Cancel" };
		int option = JOptionPane.showOptionDialog(null, panel, "Supervisor Password", JOptionPane.NO_OPTION,
				JOptionPane.PLAIN_MESSAGE, null, options, options[1]);
		if (option == 0) // pressing OK button
		{
			char[] password = pass.getPassword();
			String correctPasswordSuper = "supervisor";
			if (correctPasswordSuper.equals(new String(password))) {
				btnInputDialogActionPerformed2(null);
			} else {
				JOptionPane.showMessageDialog(null, "Wrong Password!");
			}
		}
	}

	private void btnInputDialogActionPerformed2(java.awt.event.ActionEvent evt) {
		JPanel panel = new JPanel();
		JCheckBox chckbxNewCheckBox1 = new JCheckBox("Approval");
		panel.add(chckbxNewCheckBox1);

		String[] options = new String[] { "OK", "Cancel" };
		int option = JOptionPane.showOptionDialog(null, panel, "Supervisor Password", JOptionPane.NO_OPTION,
				JOptionPane.PLAIN_MESSAGE, null, options, options[1]);

		if (option == 0) { // pressing OK button
			if (chckbxNewCheckBox1.isSelected()) {
				chckbxNewCheckBox.setSelected(true);
			} else {
				chckbxNewCheckBox.setSelected(false);
			}
		}

	}

	private void btnInputDialogActionPerformed3(java.awt.event.ActionEvent evt) {
		JPanel panel = new JPanel();
		JLabel label = new JLabel("Enter a password:");
		JPasswordField pass = new JPasswordField(10);
		panel.add(label);
		panel.add(pass);
		String[] options = new String[] { "OK", "Cancel" };
		int option = JOptionPane.showOptionDialog(null, panel, "Supervisor Password", JOptionPane.NO_OPTION,
				JOptionPane.PLAIN_MESSAGE, null, options, options[1]);
		if (option == 0) // pressing OK button
		{
			char[] password = pass.getPassword();
			String correctPasswordSuper = "supervisor";
			if (correctPasswordSuper.equals(new String(password))) {
				SupervisorPage superVisor;
				try {
					superVisor = new SupervisorPage();
					superVisor.setVisibliSuper(superVisor);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				frmKasir.dispose();
			} else {
				JOptionPane.showMessageDialog(null, "Wrong Password!");
			}
		}
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Kasir window = new Kasir();
					window.frmKasir.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Kasir() {
		kasirController = KasirController.getInstance();
		transaksi = kasirController.createTransaksi();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKasir = new JFrame();
		frmKasir.setTitle("Kasir");
		frmKasir.setBounds(100, 100, 1078, 682);
		frmKasir.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKasir.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Kasir");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 11, 1041, 14);
		frmKasir.getContentPane().add(lblNewLabel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(530, 82, 504, 355);
		frmKasir.getContentPane().add(scrollPane);
		Image image = new ImageIcon("src/resource/xiao.gif").getImage();
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("src/resource/xiao.gif").getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));

//		ImageIcon icon = new ImageIcon(this.getClass().getResource("src/resource/xiao.gif"));

		tableBarang = new JTable();
		tableBarang.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		tableBarang.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = tableBarang.getSelectedRow();
			
				String id = tableBarang.getValueAt(i, 0).toString();
				String nama = tableBarang.getValueAt(i, 1).toString();
				int stok = Integer.parseInt(tableBarang.getValueAt(i, 2).toString());
				String img = tableBarang.getValueAt(i, 3).toString();
				int price = Integer.parseInt(tableBarang.getValueAt(i, 4).toString());
				
				DetailTransaksi dt = new DetailTransaksi(transaksi.getIdTransaksi(),id, nama, stok, price);
				kasirController.saveDetailTransaksiToList(dt);
				
				DetailTransaksiTableModel dttm = new DetailTransaksiTableModel(kasirController.getAllDetailTransaksi());
				tableBarangDibeli.setModel(dttm);
			}
		});
		
		BarangTableModel barangTableModel = new BarangTableModel(kasirController.getAllBarang());
		tableBarang.setModel(barangTableModel);


//		table.getColumnModel().getColumn(3).setCellRenderer(new ImageRender());
		TableColumn tc = tableBarang.getColumnModel().getColumn(3);
		tc.setCellRenderer(new ImageRenderer());
		scrollPane.setViewportView(tableBarang);
		
		tableBarang.getColumnModel().getColumn(3).setPreferredWidth(200);
		tableBarang.setRowHeight(200);

		JLabel lblNewLabel_1 = new JLabel("Available Products");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(529, 58, 504, 14);
		frmKasir.getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Bought Products");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(35, 58, 456, 14);
		frmKasir.getContentPane().add(lblNewLabel_2);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(33, 82, 458, 356);
		frmKasir.getContentPane().add(scrollPane_1);

		tableBarangDibeli = new JTable();
		tableBarangDibeli.setFont(new Font("Tahoma", Font.PLAIN, 15));
		DetailTransaksiTableModel dttm = new DetailTransaksiTableModel(kasirController.getAllDetailTransaksi());
		tableBarangDibeli.setModel(dttm);
		
		
		tableBarangDibeli.setRowHeight(200);
		scrollPane_1.setViewportView(tableBarangDibeli);

		totalItemsField = new JTextField();
		totalItemsField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		totalItemsField.setEditable(false);
		totalItemsField.setBounds(395, 449, 96, 20);
		frmKasir.getContentPane().add(totalItemsField);
		totalItemsField.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("Total Items:");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3.setBounds(280, 449, 96, 14);
		frmKasir.getContentPane().add(lblNewLabel_3);

		totalPriceField = new JTextField();
		totalPriceField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		totalPriceField.setEditable(false);
		totalPriceField.setColumns(10);
		totalPriceField.setBounds(395, 518, 96, 20);
		frmKasir.getContentPane().add(totalPriceField);

		JLabel lblNewLabel_3_1 = new JLabel("Total Price:");
		lblNewLabel_3_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3_1.setBounds(280, 521, 96, 14);
		frmKasir.getContentPane().add(lblNewLabel_3_1);

		paidField = new JTextField();
		paidField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		paidField.setBounds(605, 468, 96, 20);
		frmKasir.getContentPane().add(paidField);
		paidField.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("Paid:");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_4.setBounds(530, 470, 49, 14);
		frmKasir.getContentPane().add(lblNewLabel_4);

		changeField = new JTextField();
		changeField.setEditable(false);
		changeField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		changeField.setColumns(10);
		changeField.setBounds(605, 518, 96, 20);
		frmKasir.getContentPane().add(changeField);

		JLabel lblNewLabel_4_1 = new JLabel("Change:");
		lblNewLabel_4_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_4_1.setBounds(530, 520, 67, 14);
		frmKasir.getContentPane().add(lblNewLabel_4_1);

		JButton finishButton = new JButton("Finish Transaction");
		finishButton.setBounds(428, 560, 169, 23);
		frmKasir.getContentPane().add(finishButton);

		textId = new JTextField();
		textId.setBounds(53, 519, 115, 20);
		frmKasir.getContentPane().add(textId);
		textId.setColumns(10);
		textId.setVisible(false);

		JLabel labelId = new JLabel("ID");
		labelId.setLabelFor(textId);
		labelId.setBounds(35, 521, 15, 17);
		frmKasir.getContentPane().add(labelId);
		labelId.setVisible(false);

		JLabel lblNewLabel_5 = new JLabel("Discount   :");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_5.setBounds(280, 486, 78, 14);
		frmKasir.getContentPane().add(lblNewLabel_5);

		diskonField = new JTextField();
		diskonField.setText("-");
		diskonField.setEditable(false);
		diskonField.setBounds(395, 487, 96, 20);
		frmKasir.getContentPane().add(diskonField);
		diskonField.setColumns(10);


		JButton submitButton = new JButton("Submit");
		submitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (KasirController.getInstance().memberIdIsValid(textId.getText())) {
					diskonField.setText("10%");
				} else {
					diskonField.setText("-");
				}
			}
		});
		submitButton.setBounds(173, 519, 89, 20);
		frmKasir.getContentPane().add(submitButton);
		submitButton.setVisible(false);

		JCheckBox memberCheckBox = new JCheckBox("Member");
		memberCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (memberCheckBox.isSelected()) {
					textId.setVisible(true);
					labelId.setVisible(true);
					submitButton.setVisible(true);

				} else {
					textId.setText("");
					textId.setVisible(false);
					labelId.setVisible(false);
					submitButton.setVisible(false);
				}
			}
		});
		memberCheckBox.setFont(new Font("Tahoma", Font.PLAIN, 13));
		memberCheckBox.setBounds(33, 467, 133, 23);
		frmKasir.getContentPane().add(memberCheckBox);

		JMenuBar menuBar = new JMenuBar();
		frmKasir.setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("Setting");
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("Help");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int jawab = JOptionPane.showOptionDialog(null, "You sure need help?", "Warning",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

				if (jawab == JOptionPane.YES_OPTION) {
					btnInputDialogActionPerformed3(null);
				}

			}
		});

		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Main Form");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main main = new Main();
				main.setVisiblimain(main);
				frmKasir.dispose();
			}
		});
		mnNewMenu.add(mntmNewMenuItem_1);
		mnNewMenu.add(mntmNewMenuItem);

		// get the column model from JTable
//		TableColumnModel model = tableBarangDibeli.getColumnModel();

		// get the 2nd column
//		TableColumn col = model.getColumn(3);
//		
//		// set the editor
//		col.setCellEditor(new MySpinnerEditor());

	}

//	private class ImageRender extends DefaultTableCellRenderer {
//
//		@Override
//		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
//				int row, int column) {
//			String photoName = value.toString();
//			ImageIcon imageIcon = new ImageIcon(new ImageIcon("src/resource/" + photoName).getImage()
//					.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
////			return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//			return new JLabel(imageIcon);
//		}
//
//	}

	class ImageRenderer extends DefaultTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			JLabel lbl = ((JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
					column));
			lbl = new JLabel(new ImageIcon((String) value));
			lbl.setIcon(new ImageIcon((String) value));
			return lbl;

		}
	}

	public static class MySpinnerEditor extends DefaultCellEditor {
		JSpinner sp;
		DefaultEditor defaultEditor;
		JTextField text;

		// Initialize the spinner
		public MySpinnerEditor() {
			super(new JTextField());
			sp = new JSpinner();
			defaultEditor = ((DefaultEditor) sp.getEditor());
			text = defaultEditor.getTextField();
		}

		// Prepare the spinner component and return it
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
				int column) {
			sp.setValue(value);
			return sp;
		}

		// Returns the current value of the spinners
		public Object getCellEditorValue() {
			return sp.getValue();
		}
	}
	
	private static class DetailTransaksiTableModel extends AbstractTableModel {
		private final String[] COLUMNS = { "ID", "Name", "Quantity" };
		private List<DetailTransaksi> dtList;

		private DetailTransaksiTableModel(List<DetailTransaksi> dtList) {
			this.dtList = dtList;
		}

		@Override
		public int getRowCount() {
			return dtList.size();
		}

		@Override
		public int getColumnCount() {
			return COLUMNS.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch (columnIndex) {
			case 0:
				return dtList.get(rowIndex).getIdBarang();
			case 1:
				return dtList.get(rowIndex).getNamaBarang();
			case 2:
				return dtList.get(rowIndex).getJumlahBarang();
			default:
				return '-';
			}
		}

		@Override
		public String getColumnName(int column) {
			return COLUMNS[column];
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (getValueAt(0, columnIndex) != null) {
				return getValueAt(0, columnIndex).getClass();
			} else {
				return Object.class;
			}
		}
	}

	
	private static class BarangTableModel extends AbstractTableModel {
		private final String[] COLUMNS = { "ID", "Item", "Stock", "IMG", "Price" };
		private List<Barang> barangList;

		private BarangTableModel(List<Barang> barangList) {
			this.barangList = barangList;
		}

		@Override
		public int getRowCount() {
			return barangList.size();
		}

		@Override
		public int getColumnCount() {
			return COLUMNS.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch (columnIndex) {
			case 0:
				return barangList.get(rowIndex).getId();
			case 1:
				return barangList.get(rowIndex).getNama();
			case 2:
				return barangList.get(rowIndex).getJumlah();
			case 3:
				return barangList.get(rowIndex).getPathGambar();
			case 4:
				return barangList.get(rowIndex).getHarga();
			default:
				return '-';
			}
		}

		@Override
		public String getColumnName(int column) {
			return COLUMNS[column];
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (getValueAt(0, columnIndex) != null) {
				return getValueAt(0, columnIndex).getClass();
			} else {
				return Object.class;
			}
		}
	}
}
