package pointOfSales.views;

import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;

import pointOfSales.views.Kasir;
import pointOfSales.views.Kasir.ImageRenderer;
import pointOfSales.controller.SupervisorController;
import pointOfSales.model.Barang;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class SupervisorPage {

	private JFrame frmSupervisorPage;
	private JTable table;
	public boolean approval = false;
	private JTextField nameAdd;
	private JTextField stockAdd;
	private JTextField nameUpdate;
	private JTextField stockUpdate;
	private SupervisorController supervisorController;
	private JTextField idUpdate;
	private JTextField fieldBuatChooser;
	private JTextField priceAdd;
	private JTextField priceUpdate;
	private JTextField picUpdate;
	private String picpath;

	/**
	 * Launch the application.
	 */

//	public void setApproval(boolean approvals) {
//		approval = approvals;
//	}
//	
//	public boolean getApproval() {
//		return approval;
//	}

	public void setVisibliSuper(SupervisorPage windows) {
		try {
			picpath = "";
			windows = new SupervisorPage();
		} catch (IOException e) {
			e.printStackTrace();
		}
		windows.frmSupervisorPage.setVisible(true);
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SupervisorPage window = new SupervisorPage();
					window.frmSupervisorPage.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws IOException
	 */
	public SupervisorPage() throws IOException {
		supervisorController = SupervisorController.getInstance();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws IOException
	 */
	private void initialize() throws IOException {
		frmSupervisorPage = new JFrame();
		frmSupervisorPage.setTitle("Supervisor Page");
		frmSupervisorPage.setBounds(100, 100, 1080, 628);
		frmSupervisorPage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSupervisorPage.getContentPane().setLayout(null);

		JLabel lblSupervisorPage = new JLabel("Supervisor Page");
		lblSupervisorPage.setHorizontalAlignment(SwingConstants.CENTER);
		lblSupervisorPage.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSupervisorPage.setBounds(10, 11, 1041, 14);
		frmSupervisorPage.getContentPane().add(lblSupervisorPage);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(456, 83, 505, 355);
		frmSupervisorPage.getContentPane().add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = table.getSelectedRow();
				idUpdate.setText(table.getValueAt(i, 0).toString());
				nameUpdate.setText(table.getValueAt(i, 1).toString());
				stockUpdate.setText(table.getValueAt(i, 2).toString());
				picUpdate.setText(table.getValueAt(i, 3).toString());
				priceUpdate.setText(table.getValueAt(i, 4).toString());
			}
		});

		table.setFont(new Font("Tahoma", Font.PLAIN, 15));

		BarangTableModel barangTableModel = new BarangTableModel(supervisorController.getAllBarang());
		table.setModel(barangTableModel);
		TableColumn tc = table.getColumnModel().getColumn(3);
		tc.setCellRenderer(new ImageRenderer());
		table.getColumnModel().getColumn(3).setPreferredWidth(200);
		table.setRowHeight(200);
		scrollPane.setViewportView(table);

		JLabel lblNewLabel = new JLabel("Available Products");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setBounds(624, 58, 148, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel);

		JButton updateButton = new JButton("Update Product");
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!idUpdate.getText().equals("")) {
					String idBarang = idUpdate.getText();
					String namaBarang = nameUpdate.getText();
					String jumlahBarang = stockUpdate.getText();
					String hargaBarang = priceUpdate.getText();
					String pathGambar = picUpdate.getText();
					

					int jawab = JOptionPane.showOptionDialog(null, "Are you sure you want to update item?",
							"Confirmation Message", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null,
							null);

					if (jawab == JOptionPane.YES_OPTION) {
						supervisorController.updateBarang(idBarang, namaBarang, jumlahBarang, hargaBarang, pathGambar);

						BarangTableModel barangTableModel = new BarangTableModel(supervisorController.getAllBarang());
						table.setModel(barangTableModel);
						TableColumn tc = table.getColumnModel().getColumn(3);
						tc.setCellRenderer(new ImageRenderer());
						table.getColumnModel().getColumn(3).setPreferredWidth(200);
						table.setRowHeight(200);

						idUpdate.setText("");
						nameUpdate.setText("");
						stockUpdate.setText("");
						priceUpdate.setText("");
					}

				} else {
					JOptionPane.showMessageDialog(null, "Please Choose Item");
				}
			}
		});
		updateButton.setBounds(68, 534, 132, 23);
		frmSupervisorPage.getContentPane().add(updateButton);

		JButton logoutButton = new JButton("Logout");
		logoutButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "You'll Log Off and back to Main page!");
//				 Login login = new Login();
//		         login.setVisiblilogin(login);
				Main main = new Main();
				main.setVisiblimain(main);
				frmSupervisorPage.dispose();

			}
		});
		logoutButton.setBounds(954, 534, 89, 23);
		frmSupervisorPage.getContentPane().add(logoutButton);

		JLabel lblNewLabel_1 = new JLabel("Add Products");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(68, 58, 105, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Products Name :");
		lblNewLabel_2.setBounds(10, 125, 102, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Stock                   : ");
		lblNewLabel_3.setBounds(10, 191, 102, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel_3);

		nameAdd = new JTextField();
		nameAdd.setBounds(122, 122, 127, 20);
		frmSupervisorPage.getContentPane().add(nameAdd);
		nameAdd.setColumns(10);

		stockAdd = new JTextField();
		stockAdd.setBounds(122, 188, 127, 20);
		frmSupervisorPage.getContentPane().add(stockAdd);
		stockAdd.setColumns(10);

		JButton addButton = new JButton("Add Product");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!nameAdd.getText().equals("") && !stockAdd.getText().equals("")) {
					String namaBarang = nameAdd.getText();
					String jumlahBarang = stockAdd.getText();
					String hargaBarang = priceAdd.getText();
					System.out.println(picpath);

					int jawab = JOptionPane.showOptionDialog(null, "Are you sure you want to add item?",
							"Confirmation Message", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null,
							null);

					if (jawab == JOptionPane.YES_OPTION) {
						supervisorController.addBarang(namaBarang, jumlahBarang, hargaBarang, picpath );
						;

						BarangTableModel barangTableModel = new BarangTableModel(supervisorController.getAllBarang());
						table.setModel(barangTableModel);
						TableColumn tc = table.getColumnModel().getColumn(3);
						tc.setCellRenderer(new ImageRenderer());
						table.getColumnModel().getColumn(3).setPreferredWidth(200);
						table.setRowHeight(200);

						nameAdd.setText("");
						stockAdd.setText("");
						priceAdd.setText("");
		
					}

				} else {
					JOptionPane.showMessageDialog(null, "Please fill item detail");
				}
			}
		});
		addButton.setBounds(55, 250, 118, 23);
		frmSupervisorPage.getContentPane().add(addButton);

		JLabel lblNewLabel_4 = new JLabel("Update Products");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setBounds(95, 362, 105, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("Products Name :");
		lblNewLabel_5.setBounds(10, 439, 102, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel("Stock                   : ");
		lblNewLabel_6.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_6.setBounds(10, 464, 102, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel_6);

		nameUpdate = new JTextField();
		nameUpdate.setBounds(122, 436, 127, 20);
		frmSupervisorPage.getContentPane().add(nameUpdate);
		nameUpdate.setColumns(10);

		stockUpdate = new JTextField();
		stockUpdate.setBounds(122, 463, 127, 20);
		frmSupervisorPage.getContentPane().add(stockUpdate);
		stockUpdate.setColumns(10);

		JLabel lblNewLabel_7 = new JLabel("ID :");
		lblNewLabel_7.setBounds(10, 408, 49, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel_7);

		idUpdate = new JTextField();
		idUpdate.setEditable(false);
		idUpdate.setBounds(122, 405, 127, 20);
		frmSupervisorPage.getContentPane().add(idUpdate);
		idUpdate.setColumns(10);

		JLabel lblNewLabel_8 = new JLabel("Image:");
		lblNewLabel_8.setBounds(10, 160, 49, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel_8);
		
		picpath = "";
		JButton addImageButton = new JButton("Add Image");
		addImageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF Images", "jpg", "gif");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File selectedFile = chooser.getSelectedFile();
//					JLabel notifSukses = new JLabel("You chose to add this file: " + selectedFile.getName());
					JLabel notifSukses = new JLabel(selectedFile.getName());
					notifSukses.setBounds(270, 160, 132, 14);
					frmSupervisorPage.getContentPane().add(notifSukses);
					frmSupervisorPage.validate();
					frmSupervisorPage.repaint();

					System.out.println("You chose to add this file: " + selectedFile.getName());

					Path newPath = Paths.get("src/resource/", selectedFile.getName());
					picpath = "src/resource/" + selectedFile.getName();
					try {
						Files.copy(selectedFile.toPath(), newPath);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

				JFrame frame = new JFrame();
				frame.getContentPane().add(chooser);
			}
		});
		addImageButton.setBounds(122, 154, 127, 23);
		frmSupervisorPage.getContentPane().add(addImageButton);
		
		JLabel lblNewLabel_9 = new JLabel("Price");
		lblNewLabel_9.setBounds(10, 220, 49, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel_9);
		
		priceAdd = new JTextField();
		priceAdd.setBounds(122, 219, 127, 20);
		frmSupervisorPage.getContentPane().add(priceAdd);
		priceAdd.setColumns(10);
		
		JLabel lblNewLabel_10 = new JLabel("Price");
		lblNewLabel_10.setBounds(10, 497, 49, 14);
		frmSupervisorPage.getContentPane().add(lblNewLabel_10);
		
		priceUpdate = new JTextField();
		priceUpdate.setBounds(122, 494, 127, 20);
		frmSupervisorPage.getContentPane().add(priceUpdate);
		priceUpdate.setColumns(10);
		
		picUpdate = new JTextField();
		picUpdate.setBounds(291, 405, 96, 20);
		frmSupervisorPage.getContentPane().add(picUpdate);
		picUpdate.setColumns(10);
		picUpdate.setVisible(false);

//		JCheckBox chckbxNewCheckBox = new JCheckBox("Approval");
//		chckbxNewCheckBox.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				if(chckbxNewCheckBox.isSelected()) {
//					setApproval(true);
//				}else {
//					setApproval(false);
//				}
//			}
//		});
//		chckbxNewCheckBox.setFont(new Font("Tahoma", Font.PLAIN, 13));
//		chckbxNewCheckBox.setBounds(286, 468, 97, 23);
//		frmSupervisorPage.getContentPane().add(chckbxNewCheckBox);
	}

	class ImageRenderer extends DefaultTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			JLabel lbl = ((JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
					column));
			lbl = new JLabel(new ImageIcon((String) value));
			lbl.setIcon(new ImageIcon((String) value));
			return lbl;

		}
	}

	private static class BarangTableModel extends AbstractTableModel {
		private final String[] COLUMNS = { "ID", "Item", "Stock", "IMG", "Price" };
		private List<Barang> barangList;

		private BarangTableModel(List<Barang> barangList) {
			this.barangList = barangList;
		}

		@Override
		public int getRowCount() {
			return barangList.size();
		}

		@Override
		public int getColumnCount() {
			return COLUMNS.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			switch (columnIndex) {
			case 0:
				return barangList.get(rowIndex).getId();
			case 1:
				return barangList.get(rowIndex).getNama();
			case 2:
				return barangList.get(rowIndex).getJumlah();
			case 3:
				return barangList.get(rowIndex).getPathGambar();
			case 4:
				return barangList.get(rowIndex).getHarga();
			default:
				return '-';
			}
		}

		@Override
		public String getColumnName(int column) {
			return COLUMNS[column];
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (getValueAt(0, columnIndex) != null) {
				return getValueAt(0, columnIndex).getClass();
			} else {
				return Object.class;
			}
		}
	}
}
