package pointOfSales.dao;

import java.util.List;
import java.util.ArrayList;

import pointOfSales.model.Transaksi;
import pointOfSales.model.DetailTransaksi;
import pointOfSales.utill.Connect;
import java.sql.*;

//singleton
public class TransaksiDAO {
	private static TransaksiDAO transaksiDAO;
	private Connect connect;
	
	private static List<Transaksi> tempListTransaksi;
	private static List<DetailTransaksi> tempListDetailTransaksi;

	
	private TransaksiDAO() {
		tempListTransaksi = new ArrayList<>();
		tempListDetailTransaksi = new ArrayList<>();
		
		connect = Connect.getConnection();
	}
	
	public static synchronized TransaksiDAO getInstance() {
		return transaksiDAO = (transaksiDAO == null) ? new TransaksiDAO() : transaksiDAO;
	}
	
	public List<DetailTransaksi> getAllDetailTransaksi() {
		return tempListDetailTransaksi;
	}
	
	public String createTransaksi() {
		//TODO
		return " ";
	}
	
	public String createDetailTransaksi() {
		//TODO
		return " ";
	}
	
	public void saveTempTransaksi() {
		
	}
	
	public void saveTempDetailTransaksi(DetailTransaksi dt) {
		tempListDetailTransaksi.add(dt);
	}
	
	
	public void saveTransaksiToDB() {
				
		for(Transaksi transaksi : tempListTransaksi) {
			String id = transaksi.getIdTransaksi();
			String totalItem = String.valueOf( transaksi.getTotalItem());
			String totalHarga = String.valueOf( transaksi.getTotalHarga());
			String jumlahPembayaran = String.valueOf( transaksi.getJumlahPembayaran());
			String jumlahKembalian = String.valueOf( transaksi.getJumlahKembalian());
			
			String query = String.format("INSERT INTO Transaksi (id,totalItem,totalHarga,jumlahPembayaran,jumlahKembalian) "
					+ "VALUES ('%s', '%s', '%s', '%s','%s');"
					, id,totalItem,totalHarga,jumlahPembayaran,jumlahKembalian);
			connect.executeUpdate(query);
		}
		
		for(DetailTransaksi dt: tempListDetailTransaksi) {
			String idTransaksi = dt.getIdTransaksi();
			String idBarang = dt.getIdBarang();
			String namaBarang = dt.getNamaBarang();
			String jumlahBarang = String.valueOf(dt.getJumlahBarang());
			String totalHarga = String.valueOf(dt.getTotalHarga());
		
			String query = String.format("INSERT INTO Transaksi (idTransaksi,idBarang,namaBarang,jumlahBarang,totalHarga) "
					+ "VALUES ('%s', '%s', '%s', '%s','%s');"
					, idTransaksi,idBarang,namaBarang,jumlahBarang,totalHarga);
			connect.executeUpdate(query);
		}
		
		tempListTransaksi.clear();
		tempListDetailTransaksi.clear();
	}
	
}