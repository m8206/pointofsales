package pointOfSales.dao;

import java.util.List;
import java.util.ArrayList;

import pointOfSales.model.Barang;
import pointOfSales.utill.Connect;
import java.sql.*;

//singleton
public class BarangDAO {
	private static BarangDAO barangDAO;
	private Connect connect;
	
	private static List<Barang> listBarang;
	
	private BarangDAO() {
		listBarang = new ArrayList<>();
		connect = Connect.getConnection();
	
		try {
			ResultSet rs = connect.executeQuery("select * from Barang");
			while(rs.next()) {
				listBarang.add(new Barang(rs.getString(1),rs.getString(2),(Integer) rs.getObject("jumlah"),(Integer) rs.getObject("harga"),rs.getString(5)));
			}
		}
		catch(Exception e){
			
		}
	}
	
	public static synchronized BarangDAO getInstance() {
		return barangDAO = (barangDAO == null) ? new BarangDAO() : barangDAO;
	}
	
	public void refreshListBarang() {
		try {
			listBarang.clear();
			ResultSet rs = connect.executeQuery("select * from Barang");
			while(rs.next()) {
				listBarang.add(new Barang(rs.getString(1),rs.getString(2),(Integer) rs.getObject("jumlah"),(Integer) rs.getObject("harga"),rs.getString(5)));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void addBarang(String nama,String jumlah,String harga,String pathGambar) {
		

		String query = String.format("INSERT INTO Barang (nama,jumlah,harga,pathGambar) "
				+ "VALUES ('%s', '%s', '%s', '%s');"
				, nama,jumlah,harga,pathGambar);
		connect.executeUpdate(query);
		refreshListBarang();	
			
	}
	
	public void updateBarang(String id,String nama,String jumlah,String harga, String pathGambar) {
		String query = String.format("UPDATE Barang SET nama = '%s',jumlah = '%s',harga = '%s', pathGambar= '%s' WHERE idBarang = '%s';",nama,jumlah,harga,pathGambar,id);
		connect.executeUpdate(query);
		
		for(Barang item:listBarang) {
			if(item.getId().equals(id) ) {
				item.setNama(nama);
				item.setJumlah(Integer.parseInt(jumlah));
				item.setHarga(Integer.parseInt(harga));
				item.setPathGambar(pathGambar);
			}
		}
	}
	
	public List<Barang> getAll(){
		return listBarang;
	}
	
	//testing function (ON DELETE)
	public void printListBarang() {
		for(Barang barang : listBarang) {
			System.out.println(barang.getId()+" "+barang.getNama()+" "+barang.getJumlah()+" "+barang.getHarga()+" "+barang.getPathGambar()+"\n");
		}
	}
	

}
