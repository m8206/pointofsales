package pointOfSales.dao;

import pointOfSales.model.Barang;
import pointOfSales.model.Member;
import pointOfSales.utill.Connect;

import java.util.List;
import java.sql.ResultSet;
import java.util.ArrayList;

public class MemberDAO {
	
	private static MemberDAO memberDAO;
	private Connect connect;
	
	private List<Member> listMember;
	
	private MemberDAO() {
		listMember = new ArrayList<>();
		connect = Connect.getConnection();
	}
	
	public static synchronized MemberDAO getInstance() {
		return memberDAO = (memberDAO == null) ? new MemberDAO() : memberDAO;
	}
	
	public Boolean validateMember(String idMember) {
		try {
			String query = String.format("select id from Member WHERE id = '%s';", idMember);
			ResultSet rs = connect.executeQuery(query);
			while(rs.next()) {
//				System.out.println(rs.getString(1));				//ON DELETE
				if(idMember.equals(rs.getString(1))) {
					return true;
					}
				}
			return false;
		}
		catch(Exception e){
			return false;
		}
	}
	
	
}
