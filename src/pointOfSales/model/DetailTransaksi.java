package pointOfSales.model;

public class DetailTransaksi {
	
	private String idTransaksi;
	private String idBarang;
	private String namaBarang;
	private int jumlahBarang;
	private int totalHarga;

	public DetailTransaksi(String idTransaksi, String idBarang, String namaBarang, int jumlahBarang, int totalHarga) {
		this.idTransaksi = idTransaksi;
		this.idBarang = idBarang;
		this.namaBarang = namaBarang;
		this.jumlahBarang = jumlahBarang;
		this.totalHarga = totalHarga;
	}

	public String getIdTransaksi() {
		return idTransaksi;
	}

	public void setIdTransaksi(String idTransaksi) {
		this.idTransaksi = idTransaksi;
	}

	public String getIdBarang() {
		return idBarang;
	}

	public void setIdBarang(String idBarang) {
		this.idBarang = idBarang;
	}

	public String getNamaBarang() {
		return namaBarang;
	}

	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}

	public int getJumlahBarang() {
		return jumlahBarang;
	}

	public void setJumlahBarang(int jumlahBarang) {
		this.jumlahBarang = jumlahBarang;
	}

	public int getTotalHarga() {
		return totalHarga;
	}

	public void setTotalHarga(int totalHarga) {
		this.totalHarga = totalHarga;
	}
	
	
	

}
