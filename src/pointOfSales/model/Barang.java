package pointOfSales.model;

public class Barang {
	
	private String id;
	private String nama;
	private int jumlah;
	private int harga;
	private String pathGambar;
	
	public Barang(String id, String nama, int jumlah, int harga, String pathGambar) {
		this.id = id;
		this.nama = nama;
		this.jumlah = jumlah;
		this.harga = harga;
		this.pathGambar = pathGambar;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getJumlah() {
		return jumlah;
	}

	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}
	
	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}
	
	public String getPathGambar() {
		return pathGambar;
	}

	public void setPathGambar(String pathGambar) {
		this.pathGambar = pathGambar;
	}
	
}
