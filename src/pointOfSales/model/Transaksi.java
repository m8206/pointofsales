package pointOfSales.model;

public class Transaksi {
	
	private String id;
	private int totalItem;
	private int totalHarga;
	private int jumlahPembayaran;
	private int jumlahKembalian;
	
	
	public Transaksi(String id,int totalItem,int totalHarga,int jumlahPembayaran,int jumlahKembalian){
		this.id = id;
		this.totalItem = totalItem;
		this.totalHarga = totalHarga;
		this.jumlahPembayaran = jumlahPembayaran;
		this.jumlahKembalian = jumlahKembalian;
	}


	public String getIdTransaksi() {
		return id;
	}


	public void setIdTransaksi(String id) {
		this.id = id;
	}


	public int getTotalItem() {
		return totalItem;
	}


	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}


	public int getTotalHarga() {
		return totalHarga;
	}


	public void setTotalHarga(int totalHarga) {
		this.totalHarga = totalHarga;
	}


	public int getJumlahPembayaran() {
		return jumlahPembayaran;
	}


	public void setJumlahPembayaran(int jumlahPembayaran) {
		this.jumlahPembayaran = jumlahPembayaran;
	}


	public int getJumlahKembalian() {
		return jumlahKembalian;
	}


	public void setJumlahKembalian(int jumlahKembalian) {
		this.jumlahKembalian = jumlahKembalian;
	}
	
	
}
