package pointOfSales.model;

public class Member {
	
	private String idMember;
	private String nama;
	
	public Member(String idMember, String nama) {
		this.idMember = idMember;
		this.nama = nama;
	}

	public String getIdMember() {
		return idMember;
	}

	public void setIdMember(String idMember) {
		this.idMember = idMember;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
	
}
