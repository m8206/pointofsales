
public class test {

}
import java.awt.BorderLayout;
import java.awt.Image;
/*from  w w w.  java2s  .  c o  m*/
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Main {
  public static void main(String[] args) {
    final JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JButton loadButton = new JButton("Display Image");
    loadButton.addActionListener(ev -> {
      JFileChooser fc = new JFileChooser(System.getProperty("user.home"));
      fc.addChoosableFileFilter(new FileNameExtensionFilter("Image files",
          new String[] { "png", "jpg", "jpeg", "gif" }));
      if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
        try {
          Image image = ImageIO.read(fc.getSelectedFile());
          if (image != null) {
            JPanel panel = new JPanel(new BorderLayout(10, 10));
            panel.add(new JLabel(fc.getSelectedFile().toString()),
                BorderLayout.NORTH);
            panel.add(new JLabel(new ImageIcon(image)));
            JOptionPane.showMessageDialog(frame, panel);
          }
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    });

    frame.add(loadButton);
    frame.pack();
    frame.setLocationByPlatform(true);
    frame.setVisible(true);
  }
}